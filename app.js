import express from 'express';
import bodyParser from 'body-parser';

import Router from './routes';

const app = express();

app.set('port', 7000);
app.use('/assets', express.static('node_modules/bootstrap/dist/'));
app.use('/public', express.static('public/'));
app.use(bodyParser.json());
app.set('views', './views');
app.set('view engine', 'jade');

Router(app);

export default app;
