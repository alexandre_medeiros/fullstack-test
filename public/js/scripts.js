$(function() {

    $(document).on('click', '.submit', function() {

        $.ajax({
            type: 'POST',
            data: JSON.stringify({word: $('input[name="word"]').val()}),
            contentType: 'application/json',
            url: 'http://localhost:7000/',
            beforeSend: function() {
                $('.alert').removeClass('alert-danger').removeClass('alert-success').find('p').empty();
            },
            success: function(result) {
                $('.alert').removeClass('alert-danger').addClass('alert-success').find('p').html( 'It`s a Palindrome' );
            },
            error: function() {
                $('.alert').removeClass('alert-success').addClass('alert-danger').find('p').html( 'It`s not a Palindrome' );
            }
        });

    });

});
