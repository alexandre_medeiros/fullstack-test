describe('Routes', () => {

  describe('Route POST / {"word": "aba"}', () => {
    it('isPalindrome: deve retornar statusCode 200', (done) => {
      request
.post('/')
.send({"word": "aba"})
.end((err, res) => {
  expect(res.status).to.be.eql(200);

  done(err);
});
    });
  });

  describe('Route POST / {"word": "macaco"}', () => {
      it('isNotPalindrome: deve retornar statusCode 400', (done) => {
          request
              .post('/')
              .send({"word": "macaco"})
              .end((err, res) => {
                  expect(res.status).to.be.eql(400);

                  done(err);
              });
      });
  });

    describe('Route POST / {"word": "a man a plan a canal panama"}', () => {
        it('isPalindrome: deve retornar statusCode 200', (done) => {
            request
                .post('/')
                .send({"word": "a man a plan a canal panama"})
                .end((err, res) => {
                    expect(res.status).to.be.eql(200);

                    done(err);
                });
        });
    });

});
